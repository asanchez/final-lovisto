from django.test import TestCase

from .parsers.html_parser import html_parser
from .parsers.yt_parser import yt_parser
from .parsers.aemet_parser import aemet_parser
from .parsers.wiki_parser import wiki_parser

from .parsers.html_parser import info_html
from .parsers.yt_parser import info_yt
from .parsers.aemet_parser import info_aemet
from .parsers.wiki_parser import info_wiki

# Create your tests here.

class Parsers_Test(TestCase):
    def test_parser_html(self):
        url = "https://www.python.org/"
        (title, image) = html_parser(url)
        self.assertEquals(title, "Welcome to Python.org")
        self.assertEquals(image, "https://www.python.org/static/opengraph-icon-200x200.png")
        #print(info_html(url))

    def test_parser_html_2(self):
        url = "https://www.aulavirtual.urjc.es/"
        (title, image) = html_parser(url)
        self.assertIn("Aula Virtual | Universidad Rey Juan Carlos", title)
        self.assertIsNone(image)
        #print(info_html(url))

    def test_parser_yt(self):
        url = "https://www.youtube.com/watch?v=IfoSqaxJsAM"
        (title, author, iframe) = yt_parser(url)
        self.assertEquals(title, "Presentación de la asignatura. Back-end y front-end.")
        self.assertEquals(author, "CursosWeb")
        self.assertIn("accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope;", iframe)
        #print(info_yt(url))

    def test_parser_aemet(self):
        url = "http://www.aemet.es/es/eltiempo/prediccion/municipios/getafe-id28065"
        (municipio, provincia, copyright, prediccion) = aemet_parser(url)
        self.assertEquals(municipio, "Getafe")
        self.assertEquals(provincia, "Madrid")
        self.assertIn("citando a AEMET", copyright)
        #print(info_aemet(url))

    def test_parser_wikipedia(self):
        site = "https://es.wikipedia.org/wiki/Astronauta"
        (texto, imagen, copyright) = wiki_parser(site)
        self.assertIn("cosmonauta o taikonauta es el término que designa a", texto)
        self.assertEquals(imagen, "https://upload.wikimedia.org/wikipedia/commons/thumb/9/91/Bruce_McCandless_II_during_EVA_in_1984.jpg/100px-Bruce_McCandless_II_during_EVA_in_1984.jpg")

    def test_parser_wikipedia_2(self):
        site = "https://es.wikipedia.org/wiki/Concierto"
        (texto, imagen, copyright) = wiki_parser(site)
        self.assertIn("concierto", texto)
        self.assertIn(".jpg", imagen)
        #print(info_wiki(site))


