from django.db import models

# Create your models here.

class Contenidos (models.Model):
    titulo = models.CharField(max_length=128)
    url = models.TextField()
    descripcion = models.TextField()
    usuario = models.TextField()
    votos_positivos = models.IntegerField()
    votos_negativos = models.IntegerField()
    fecha = models.DateTimeField()
    info = models.TextField()

    def __str__(self):
        return str(self.id) + ": " + self.titulo


# Relacion 1 a n: un contenido puede tener n comentarios
class Comentario (models.Model):
    contenido = models.ForeignKey(Contenidos, on_delete = models.CASCADE)
    cuerpo = models.TextField()
    fecha = models.DateTimeField()
    usuario = models.TextField()

    def __str__(self):
        return str(self.id) + ": " + self.titulo

class Voto (models.Model):
    TIPOS = [
        ('P', 'positivo'),
        ('N', 'negativo')
    ]
    contenido = models.ForeignKey(Contenidos, on_delete = models.CASCADE)
    usuario = models.TextField()
    tipo = models.CharField(max_length=1, choices=TIPOS)

    def __str__(self):
        return str(self.id) + ": " + self.tipo
