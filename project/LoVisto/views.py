from django.http import HttpResponse
from django.template import loader
from django.utils import timezone
from django.shortcuts import render

from django.contrib.auth import logout
from django.contrib.auth import authenticate
from django.contrib.auth import login
from django.contrib.auth.decorators import login_required

from .parsers.get_info import get_info_from_url

from .models import Contenidos
from .models import Comentario
from .models import Voto

from .forms import ContenidoForm
from .forms import ComentarioForm

from django.http import JsonResponse
from django.core import serializers


# Funciones auxiliares

def loginout(request):
    if request.method == "POST":
        if request.POST['action']=='Logout':
            logout(request)
        if request.POST['action'] == 'Login':
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)

def get_last_from_list(n, lista):
    return lista.objects.all().order_by('-fecha')[:n]

def get_last_from_list_5(n, lista, username):
    lista2 = lista.objects.all().filter(usuario = username)
    return lista2.order_by('-id')[:n]

def addVotoPositivo(username, contenido):
    done = False

    for voto in contenido.voto_set.all():
        if voto.usuario == username:
            done = True
            if voto.tipo == 'P':
                return;
            else:
                contenido.votos_positivos = contenido.votos_positivos + 1
                contenido.votos_negativos = contenido.votos_negativos - 1
                voto.tipo = 'P'
                contenido.save()
                voto.save()
    if not done:
        contenido.votos_positivos = contenido.votos_positivos + 1
        newVoto = Voto(contenido=contenido, usuario=username, tipo='P')
        newVoto.save()
        contenido.save()

def addVotoNegativo(username, contenido):
    done = False

    for voto in contenido.voto_set.all():
        if voto.usuario == username:
            done = True
            if voto.tipo == 'N':
                return;
            else:
                contenido.votos_positivos = contenido.votos_positivos - 1
                contenido.votos_negativos = contenido.votos_negativos + 1
                voto.tipo = 'N'
                contenido.save()
                voto.save()
    if not done:
        contenido.votos_negativos = contenido.votos_negativos + 1
        newVoto = Voto(contenido=contenido, usuario=username, tipo='N')
        newVoto.save()
        contenido.save()

def voto_usuario(username, contenido):
    voto = Voto.objects.all().filter(contenido=contenido).filter(usuario=username)
    if voto:
        return voto[0]
    else:
        return None


# Vistas

def home(request):
    loginout(request)
    if request.method == "POST":
        form = ContenidoForm(request.POST)
        if form.is_valid():
            content = form
            content.instance.fecha = timezone.now()
            content.instance.usuario = request.user.username
            content.instance.votos_positivos = 0
            content.instance.votos_negativos = 0
            content.instance.info = get_info_from_url(request.POST['url'])
            content.save()

    template = loader.get_template('LoVisto/home.html')
    context = { 'content_list': Contenidos.objects.all(),
                'form': ContenidoForm(),
                'user': request.user.username,
                'autenticado': request.user.is_authenticated,
                'last_3': get_last_from_list(3, Contenidos),
                'last_5': get_last_from_list_5(5, Contenidos, request.user.username)}

    return HttpResponse(template.render(context, request))

def show_content(request, id):
    loginout(request)
    try:
        contenido = Contenidos.objects.get(id=id)
    except Contenidos.DoesNotExist:
        context = {'autenticado': request.user.is_authenticated}
        return render(request, 'LoVisto/error.html', context)

    if request.method == "POST":
        if request.POST['action'] == 'comment':
            form = ComentarioForm(request.POST)
            if form.is_valid():
                content = form
                content.instance.fecha = timezone.now()
                content.instance.usuario = request.user.username
                content.instance.contenido = contenido
                content.save()
        elif request.POST['action'] == 'positivo':
            addVotoPositivo(request.user.username, contenido)
        elif request.POST['action'] == 'negativo':
            addVotoNegativo(request.user.username, contenido)

    template = loader.get_template('LoVisto/show_content.html')
    context = { 'content': contenido,
                'form': ComentarioForm(),
                'autenticado': request.user.is_authenticated,
                'comentarios': contenido.comentario_set.all,
                'user': request.user.username,
                'votou': voto_usuario(request.user.username, contenido),
                'last_3': get_last_from_list(3, Contenidos)}

    return HttpResponse(template.render(context, request))


@login_required
def user(request):
    loginout(request)

    template = loader.get_template('LoVisto/user.html')
    context = {'autenticado': request.user.is_authenticated,
               'user': request.user.username,
               'last_3': get_last_from_list(3, Contenidos),
               'content_list': Contenidos.objects.all(),
               'comentarios': Comentario.objects.all(),
               'votos': Voto.objects.all()}
    print(Voto.objects.all())
    return HttpResponse(template.render(context, request))


def about(request):
    loginout(request)

    template = loader.get_template('LoVisto/about.html')
    context = {'autenticado': request.user.is_authenticated,
               'user': request.user.username,
               'last_3': get_last_from_list(3, Contenidos)}

    return HttpResponse(template.render(context, request))


def contributions(request):
    if request.GET.get("format")=="json/":
        queryset = Contenidos.objects.filter().values()
        return JsonResponse({"Contenidos": list(queryset)})
    if request.GET.get("format")=="xml/":
        template = loader.get_template('LoVisto/template_xml.html')
        context = {'entries': Contenidos.objects.all(),}
        return HttpResponse(template.render(context, request))


    loginout(request)

    template = loader.get_template('LoVisto/contributions.html')
    context = {'content_list': Contenidos.objects.all(),
               'autenticado': request.user.is_authenticated,
               'user': request.user.username,
               'last_3': get_last_from_list(3, Contenidos)}

    return HttpResponse(template.render(context, request))

