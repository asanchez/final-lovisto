# Final Lo Visto

Práctica final del curso 2020/21

## Datos

* Nombre:                     Amelia Sánchez Aparicio
* Titulación:                 DG ITT + IAA (tecnologías telecomunicación + aeronavegación)
* Usuario de laboratorio:     asanchez
* Correo universidad:         a.sanchezap.2017@alumnos.urjc.es
* Despliegue (url):           http://muny.pythonanywhere.com/LoVisto/
* Video básico (url):         https://www.youtube.com/watch?v=KUDPvkB7y7o

## Cuenta Admin Site
* usuario/contraseña        admin/admin

## Cuentas usuarios
* usuario/contraseña        muny/contraseña

## Resumen parte obligatoria
La práctica final de la asignatura consiste en la creación de una aplicación web, llamada “LoVisto”, que permitirá gestionar aportaciones de los usuarios, que serán enlaces (URLs) a vı́deos, noticias y otra información que los usuarios vayan viendo por la red y les resulte interesante.

La página web se ha diseñado en Django con gestión de usuarios, de contenidos en una base de datos SQLite y se ha desplegado en PythonAnywhere.

## Lista partes opcionales
* Nombre parte:             Favicon
